import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alert App Bar'),
      ),
      body: Center(
        child: Container(
            child: ElevatedButton(
          child: Text('Notificaciones'),
          onPressed: () {
            _showAlert(context);
          },
          style: ElevatedButton.styleFrom(
              primary: Colors.redAccent, shape: StadiumBorder()),
        )),
      ),
    );
  }

//metodo para el boton
  Widget _showAlert(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Notificaciones...'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                    "¿Desea recibir notificaciones? Serán muy pocas de verdad :)"),
                FlutterLogo(
                  size: 100.00,
                )
              ],
            ),
            actions: <Widget>[
              TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('Cancelar')),
              TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('Aceptar'))
            ],
          );
        });
  }
}
